# Hass Remote Media Player

[Home Assistant](https://www.home-assistant.io/) custom integration that wraps remote devices in Home Assistant to create
a media player entity.

In Home Assistant, there is a generic [remote](https://www.home-assistant.io/integrations/remote/) integration that can
be used do execute RF/IR/remote commands on devices like TVs, soundbars etc.

Unfortunately, the generic integration does not have any built-in way to remember the state of the device, so one does
not simply know if the entity is powered on or muted. This is where this custom integration helps - it wraps the generic
remote commands and adds the state to the device, remembering whether the user powered the device previously - or how many
times the `volume up` command should be called to increase the volume to 100%.

Of course, there are situations where the remembered state of the device will not match the reality - for example, somebody
turned the TV off without using Home Assistant. This problem can be resolved by specifying the `template` that can - for
example - check the energy consumption for TV device to determine whether it is powered on.

## Installation

### Manual

All you have to do is place the [remote_media_player](custom_components/remote_media_player) directory inside your
`/config/custom_components/` directory (if the `custom_components` does not exist, simply create it).

### HACS

Currently [HACS](https://hacs.xyz/) integration does not exist. If the custom component will gain popularity
we will try to add it to the official HACS repository.

## Usage

### Config flow

This custom component does support GUI installation. In your Home Assistant instance please search for Remote Media Player
integration or simply click on button below
(if you have configured the [My Home Assistant](https://my.home-assistant.io/)) to add it.

[![Open your Home Assistant instance and start setting up a new integration.](https://my.home-assistant.io/badges/config_flow_start.svg)](https://my.home-assistant.io/redirect/config_flow_start/?domain=remote_media_player)

### YAML

The YAML configuration is currently not supported.

## Debugging

To enable debug logs please set up the Home Assistant logger:

```yaml
custom_components.remote_media_player: debug
```
