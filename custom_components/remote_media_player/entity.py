from __future__ import annotations

import logging

from homeassistant.config_entries import ConfigEntry
from homeassistant.const import CONF_NAME
from homeassistant.core import callback, HomeAssistant, Event
from homeassistant.exceptions import HomeAssistantError, TemplateError
from homeassistant.helpers.device_registry import DeviceInfo
from homeassistant.helpers.entity import Entity
from homeassistant.helpers.entity import generate_entity_id
from homeassistant.helpers.event import (async_track_template_result, EventStateChangedData,
                                         TrackTemplate,
                                         TrackTemplateResult,
                                         TrackTemplateResultInfo, )
from homeassistant.helpers.template import Template


from .const import DOMAIN, CONF_REMOTE_ENTITY, CONF_DEVICE_ID, CONF_POWER_CHECK_TEMPLATE, CONF_COMMANDS, \
    CONF_VOLUME_STEP, CONF_SOURCES

_LOGGER = logging.getLogger(__name__)


class RemoteMediaPlayerBaseEntity(Entity):
    """Remote Media Player Base Entity."""

    _attr_name = None
    _attr_has_entity_name = True
    _attr_should_poll = False
    _use_power_template = False
    _attr_is_on = False
    _attr_source_list = []

    def __init__(self, hass: HomeAssistant, config_entry: ConfigEntry) -> None:
        self.hass = hass
        """Initialize the entity."""
        self._reload_config(hass, config_entry)

        self._attr_unique_id = "irmp" + self._name
        self._attr_is_on = False
        self._attr_is_muted = False
        self._attr_available = True
        self._callbacks: list[TrackTemplateResultInfo] = []

        self._attr_device_info = DeviceInfo(
            identifiers={(DOMAIN, self._attr_unique_id)},
            name=self._name
        )

    def _reload_config(self, hass: HomeAssistant, config_entry: ConfigEntry):
        self._name = config_entry.data.get(CONF_NAME)
        self.entity_id = self.generate_entity_id(hass)
        _LOGGER.debug("Reloading the config for the entity '%s'...", self.entity_id)
        self._remote_entity = config_entry.data.get(CONF_REMOTE_ENTITY)
        self._device_name = config_entry.data.get(CONF_DEVICE_ID)
        self._commands = {k: v for k, v in config_entry.data.get(CONF_COMMANDS).items() if v}
        self._attr_source_list = config_entry.data.get(CONF_SOURCES)
        self._volume_step = config_entry.data.get(CONF_VOLUME_STEP) / 100
        _LOGGER.debug("Supported commands for entity '%s': %s", self.entity_id, self._commands.values())
        _LOGGER.debug("Supported sources for entity '%s': %s", self.entity_id, self._attr_source_list)

        self._power_template = None
        if config_entry.data.get(CONF_POWER_CHECK_TEMPLATE):
            self._use_power_template = True
            self._power_template = Template(config_entry.data.get(CONF_POWER_CHECK_TEMPLATE), hass)

    def generate_entity_id(self, hass: HomeAssistant):
        return generate_entity_id("media_player.{}", self._name, None, hass)

    async def async_added_to_hass(self) -> None:
        """Register callbacks."""
        _LOGGER.info("Adding the entity '%s' to Home assistant...", self.entity_id)
        self.async_write_ha_state()
        if self._use_power_template:
            self._register_power_template()

    def _register_power_template(self) -> None:
        _LOGGER.debug(
            "The entity '%s' has power template configured. Registering callbacks for updates...", self.entity_id)
        info = async_track_template_result(
            self.hass,
            [TrackTemplate(self._power_template, None)],
            self.power_template_changed,
        )
        self._callbacks.append(info)
        info.async_refresh()
        self.async_on_remove(info.async_remove)

    async def async_update(self) -> None:
        for call in self._callbacks:
            call.async_refresh()

    @callback
    def power_template_changed(self, _event: Event[EventStateChangedData] | None,
                               updates: list[TrackTemplateResult]):
        last_update = updates.pop()

        self.async_write_ha_state()
        if isinstance(last_update, TemplateError):
            _LOGGER.error(
                "TemplateError('%s') while processing template '%s' in entity '%s'",
                last_update,
                last_update.template,
                self.entity_id,
            )
        else:
            self._attr_is_on = True if last_update.result else False
            _LOGGER.debug("Received power template change for the entity '%s': %s", self.entity_id, last_update.result)
            self.async_write_ha_state()

    async def async_will_remove_from_hass(self) -> None:
        """Remove callbacks."""
        _LOGGER.debug("Removing the entity '%s' from Home Assistant...", self.entity_id)
        self.async_write_ha_state()

    async def _execute_command(self, command_name: str) -> None:
        """Send a key press"""
        if command_name in self._commands:
            service_data = {'entity_id': self._remote_entity, 'command': self._commands[command_name],
                            'device': self._device_name}
            await self.hass.services.async_call('remote', 'send_command', service_data)
        else:
            raise HomeAssistantError("Command not supported: " + command_name)

    async def _change_source(self, source_name: str) -> None:
        """Change source on the device"""
        if source_name in self._attr_source_list:
            service_data = {'entity_id': self._remote_entity, 'command': source_name, 'device': self._device_name}
            await self.hass.services.async_call('remote', 'send_command', service_data)
        else:
            raise HomeAssistantError("Source not supported: " + source_name)

    def _is_command_supported(self, command) -> bool:
        return command in self._commands and self._commands[command] is not None

    def is_power_template_defined(self) -> bool:
        return self._use_power_template
