"""Config flow for Remote Media Player integration."""
from __future__ import annotations

from typing import Any

import voluptuous as vol
from homeassistant import config_entries

from homeassistant.const import CONF_NAME
from homeassistant.config_entries import (
    ConfigFlow, ConfigEntry,
)
from homeassistant.core import callback
from homeassistant.data_entry_flow import FlowResult

from .const import DOMAIN, CONF_DEVICE_ID, CONF_VOLUME_STEP, CONF_COMMAND_POWER_ON, CONF_COMMAND_POWER_OFF, \
    CONF_COMMAND_VOLUME_UP, CONF_COMMAND_VOLUME_DOWN, CONF_COMMAND_VOLUME_MUTE, CONF_POWER_CHECK_TEMPLATE, \
    CONF_REMOTE_ENTITY, CONF_COMMAND_PAUSE, CONF_COMMAND_PLAY, CONF_COMMAND_STOP, CONF_COMMAND_NEXT_TRACK, \
    CONF_COMMAND_PREVIOUS_TRACK, CONF_COMMANDS, CONF_SOURCES
from homeassistant.helpers.selector import (
    TemplateSelector, TemplateSelectorConfig, EntitySelector, EntitySelectorConfig,
    NumberSelector, NumberSelectorConfig, NumberSelectorMode, SelectSelector, SelectSelectorConfig
)

STEP_USER_DATA_SCHEMA = vol.Schema({
    vol.Required("name"): str,
    vol.Required("remote"): EntitySelector(
        EntitySelectorConfig(
            multiple=False,
            domain="remote"
        )
    ),
    vol.Required("device_id"): str
})

STEP_COMMANDS_DATA_SCHEMA = vol.Schema({
    vol.Optional(CONF_COMMAND_POWER_ON): str,
    vol.Optional(CONF_COMMAND_POWER_OFF): str,
    vol.Optional(CONF_COMMAND_VOLUME_UP): str,
    vol.Optional(CONF_COMMAND_VOLUME_DOWN): str,
    vol.Optional(CONF_COMMAND_VOLUME_MUTE): str,
    vol.Optional(CONF_COMMAND_PAUSE): str,
    vol.Optional(CONF_COMMAND_PLAY): str,
    vol.Optional(CONF_COMMAND_STOP): str,
    vol.Optional(CONF_COMMAND_NEXT_TRACK): str,
    vol.Optional(CONF_COMMAND_PREVIOUS_TRACK): str,
})

STEP_CONFIGURATION_DATA_SCHEMA = vol.Schema({
    vol.Optional(CONF_VOLUME_STEP, default=1): NumberSelector(
        NumberSelectorConfig(
            min=0.01,
            max=100,
            step=1,
            mode=NumberSelectorMode.SLIDER
        )
    ),
    vol.Optional(CONF_POWER_CHECK_TEMPLATE): TemplateSelector(
        TemplateSelectorConfig()
    ),
})

STEP_SOURCES_DATA_SCHEMA = vol.Schema({
    vol.Optional(CONF_SOURCES, default=[]): SelectSelector(
        SelectSelectorConfig(
            multiple=True,
            custom_value=True,
            options=[]
        )
    )
})


class RemoteMediaPlayerConfigFlow(ConfigFlow, domain=DOMAIN):
    """Handle a config flow for Remote Media Player."""

    VERSION = 1

    def __init__(self) -> None:
        """Initialize a new RemoteMediaPlayerConfigFlow."""
        self.name: str | None = None
        self.remote: str | None = None
        self.device_id: str | None = None
        self.commands: dict | None = None
        self.volume_step: float | None = None
        self.power_on_template: str | None = None
        self.sources: list[str] | None = None

    async def async_step_user(
            self, user_input: dict[str, Any] | None = None
    ) -> FlowResult:
        """Handle the initial step."""
        if user_input is not None:
            self.name = user_input["name"]
            assert self.name
            self.remote = user_input["remote"]
            assert self.remote
            self.device_id = user_input["device_id"]
            assert self.remote
            return await self.async_step_commands()

        return self.async_show_form(
            step_id="user",
            data_schema=STEP_USER_DATA_SCHEMA
        )

    async def async_step_commands(
            self,
            user_input: dict[str, Any] | None = None
    ) -> FlowResult:
        """Handles the commands list step"""
        if user_input is not None:
            self.commands = user_input
            return await self.async_step_sources()

        return self.async_show_form(
            step_id="commands",
            data_schema=STEP_COMMANDS_DATA_SCHEMA,
        )

    async def async_step_sources(
            self,
            user_input: dict[str, Any] | None = None) -> FlowResult:
        """Handles the sources list step"""
        if user_input is not None:
            self.sources = user_input[CONF_SOURCES]
            return await self.async_step_configuration()

        return self.async_show_form(
            step_id="sources",
            data_schema=STEP_SOURCES_DATA_SCHEMA,
        )

    async def async_step_configuration(
            self,
            user_input: dict[str, Any] | None = None) -> FlowResult:
        """Handles additional information step"""
        if user_input is not None:
            self.volume_step = user_input.get(CONF_VOLUME_STEP)
            self.power_on_template = user_input.get(CONF_POWER_CHECK_TEMPLATE)
            return self.async_create_entry(
                title=self.name,
                data={
                    CONF_NAME: self.name,
                    CONF_REMOTE_ENTITY: self.remote,
                    CONF_DEVICE_ID: self.device_id,
                    CONF_COMMANDS: self.commands,
                    CONF_VOLUME_STEP: self.volume_step,
                    CONF_POWER_CHECK_TEMPLATE: self.power_on_template,
                    CONF_SOURCES: self.sources
                },
            )

        return self.async_show_form(
            step_id="configuration",
            data_schema=STEP_CONFIGURATION_DATA_SCHEMA,
        )

    @staticmethod
    @callback
    def async_get_options_flow(
            config_entry: ConfigEntry,
    ) -> config_entries.OptionsFlow:
        """Create the options flow."""
        return RemoteMediaPlayerOptionsFlowHandler(config_entry)


class RemoteMediaPlayerOptionsFlowHandler(config_entries.OptionsFlow):
    """Remote Media Player options flow."""

    def __init__(self, config_entry: config_entries.ConfigEntry) -> None:
        """Initialize options flow."""
        self.config_entry = config_entry

    async def async_step_init(
            self, user_input: dict[str, Any] | None = None
    ) -> FlowResult:
        """Manage the options."""
        if user_input is not None:
            self._update_config_entry({
                CONF_REMOTE_ENTITY: user_input[CONF_REMOTE_ENTITY],
                CONF_DEVICE_ID: user_input[CONF_DEVICE_ID],
                CONF_VOLUME_STEP: user_input[CONF_VOLUME_STEP],
                CONF_POWER_CHECK_TEMPLATE: user_input[CONF_POWER_CHECK_TEMPLATE],
            })
            return await self.async_step_commands()
        return self.async_show_form(
            step_id="init",
            data_schema=vol.Schema({
                vol.Required(CONF_REMOTE_ENTITY, default=self.config_entry.data[CONF_REMOTE_ENTITY]): EntitySelector(
                    EntitySelectorConfig(
                        multiple=False,
                        domain="remote"
                    )
                ),
                vol.Required(CONF_DEVICE_ID, default=self.config_entry.data[CONF_DEVICE_ID]): str,
                vol.Optional(CONF_VOLUME_STEP, default=self.config_entry.data[CONF_VOLUME_STEP]): NumberSelector(
                    NumberSelectorConfig(
                        min=0.01,
                        max=100,
                        step=1,
                        mode=NumberSelectorMode.SLIDER
                    )
                ),
                vol.Optional(CONF_POWER_CHECK_TEMPLATE,
                             default=self.config_entry.data[CONF_POWER_CHECK_TEMPLATE]): TemplateSelector(
                    TemplateSelectorConfig()
                ),
            })
        )

    async def async_step_commands(
            self, user_input: dict[str, Any] | None = None
    ) -> FlowResult:
        if user_input is not None:
            self._update_config_entry({
                CONF_COMMANDS: user_input
            })
            return await self.async_step_sources()
        return self.async_show_form(
            step_id="commands",
            data_schema=vol.Schema({
                vol.Optional(CONF_COMMAND_POWER_ON,
                             default=self._get_default_for_configured_command(CONF_COMMAND_POWER_ON)): str,
                vol.Optional(CONF_COMMAND_POWER_OFF,
                             default=self._get_default_for_configured_command(CONF_COMMAND_POWER_OFF)): str,
                vol.Optional(CONF_COMMAND_VOLUME_UP,
                             default=self._get_default_for_configured_command(CONF_COMMAND_VOLUME_UP)): str,
                vol.Optional(CONF_COMMAND_VOLUME_DOWN,
                             default=self._get_default_for_configured_command(CONF_COMMAND_VOLUME_DOWN)): str,
                vol.Optional(CONF_COMMAND_VOLUME_MUTE,
                             default=self._get_default_for_configured_command(CONF_COMMAND_VOLUME_MUTE)): str,
                vol.Optional(CONF_COMMAND_PAUSE,
                             default=self._get_default_for_configured_command(CONF_COMMAND_PAUSE)): str,
                vol.Optional(CONF_COMMAND_PLAY,
                             default=self._get_default_for_configured_command(CONF_COMMAND_PLAY)): str,
                vol.Optional(CONF_COMMAND_STOP,
                             default=self._get_default_for_configured_command(CONF_COMMAND_STOP)): str,
                vol.Optional(CONF_COMMAND_NEXT_TRACK,
                             default=self._get_default_for_configured_command(CONF_COMMAND_NEXT_TRACK)): str,
                vol.Optional(CONF_COMMAND_PREVIOUS_TRACK,
                             default=self._get_default_for_configured_command(CONF_COMMAND_PREVIOUS_TRACK)): str,
            })
        )

    async def async_step_sources(
            self, user_input: dict[str, Any] | None = None
    ) -> FlowResult:
        if user_input is not None:
            self._update_config_entry({
                CONF_SOURCES: user_input[CONF_SOURCES]
            })
            return self.async_create_entry(
                title="", data={}
            )

        return self.async_show_form(
            step_id="sources",
            data_schema=vol.Schema({
                vol.Optional(CONF_SOURCES, default=self.config_entry.data[CONF_SOURCES]): SelectSelector(
                    SelectSelectorConfig(
                        multiple=True,
                        custom_value=True,
                        options=self.config_entry.data[CONF_SOURCES]
                    )
                )
            })
        )

    def _update_config_entry(self, user_data: dict) -> None:
        new_data = self.config_entry.data.copy()
        new_data.update(user_data)
        self.hass.config_entries.async_update_entry(
            self.config_entry,
            data=new_data,
        )

    def _get_default_for_configured_command(self, command) -> str:
        value = self.config_entry.data[CONF_COMMANDS].get(command)
        return value if value and len(value) > 0 else ""
