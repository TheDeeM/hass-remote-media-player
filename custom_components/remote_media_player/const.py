from __future__ import annotations
from typing import Final

DOMAIN: Final = "remote_media_player"

CONF_DEVICE_ID: Final = "device_id"
CONF_REMOTE_ENTITY: Final = "remote_entity"
CONF_COMMANDS: Final = "commands"
CONF_SOURCES: Final = "sources"
CONF_VOLUME_STEP: Final = "volume_step"
CONF_COMMAND_POWER_ON: Final = "power_on"
CONF_COMMAND_POWER_OFF: Final = "power_off"
CONF_COMMAND_VOLUME_UP: Final = "volume_up"
CONF_COMMAND_VOLUME_DOWN: Final = "volume_down"
CONF_COMMAND_VOLUME_MUTE: Final = "mute"
CONF_COMMAND_PAUSE: Final = "pause"
CONF_COMMAND_PLAY: Final = "play"
CONF_COMMAND_STOP: Final = "stop"
CONF_COMMAND_NEXT_TRACK: Final = "next_track"
CONF_COMMAND_PREVIOUS_TRACK: Final = "previous_track"
CONF_POWER_CHECK_TEMPLATE: Final = "power_check_template"

STORAGE_KEY: Final = DOMAIN
STORAGE_VERSION: Final = 1
STORAGE_CURRENT_VOLUME: Final = "volume_level"
STORAGE_IS_MUTED: Final = "is_muted"
STORAGE_IS_POWERED: Final = "is_powered"
STORAGE_SELECTED_SOURCE: Final = "selected_source"
