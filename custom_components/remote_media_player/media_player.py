from __future__ import annotations

import asyncio
import functools
import logging
import operator

from homeassistant.components.media_player import (
    MediaPlayerDeviceClass,
    MediaPlayerEntity,
    MediaPlayerEntityFeature,
    MediaPlayerState
)
from homeassistant.config_entries import ConfigEntry
from homeassistant.const import CONF_COMMAND_STOP
from homeassistant.core import HomeAssistant
from homeassistant.helpers.entity_platform import AddEntitiesCallback
from homeassistant.helpers.storage import Store

from .const import CONF_COMMAND_POWER_ON, CONF_COMMAND_VOLUME_MUTE, CONF_COMMAND_VOLUME_UP, \
    CONF_COMMAND_VOLUME_DOWN, CONF_COMMAND_PLAY, CONF_COMMAND_PAUSE, CONF_COMMAND_NEXT_TRACK, \
    CONF_COMMAND_PREVIOUS_TRACK, CONF_COMMAND_POWER_OFF, STORAGE_CURRENT_VOLUME, STORAGE_IS_MUTED, STORAGE_IS_POWERED, \
    STORAGE_SELECTED_SOURCE, STORAGE_VERSION, STORAGE_KEY
from .entity import RemoteMediaPlayerBaseEntity

_LOGGER = logging.getLogger(__name__)

PARALLEL_UPDATES = 0


async def async_setup_entry(
        hass: HomeAssistant,
        config_entry: ConfigEntry,
        async_add_entities: AddEntitiesCallback,
) -> None:
    """Set up the Remote Media Player entity based on a config entry."""
    store = Store(hass=hass, version=STORAGE_VERSION, key=STORAGE_KEY, private=True)
    async_add_entities([RemoteMediaPlayerEntity(hass, config_entry, store)])


class RemoteMediaPlayerEntity(RemoteMediaPlayerBaseEntity, MediaPlayerEntity):
    """Remote Media Player Entity."""

    _attr_assumed_state = True
    _attr_device_class = MediaPlayerDeviceClass.TV
    _attr_supported_features: MediaPlayerEntityFeature
    _attr_volume_level = 0
    _attr_is_volume_muted = False

    def __init__(self, hass: HomeAssistant, config_entry: ConfigEntry, store: Store) -> None:
        """Initialize the entity."""
        super().__init__(hass, config_entry)
        self._store = store
        self._attr_supported_features = self._get_supported_features()
        _LOGGER.debug("Calculated features for the entity '%s': %s", self.entity_id, self._attr_supported_features)
        _LOGGER.debug("Test '%s': %s", self._name, self._attr_is_on)
        self._temp_lock = asyncio.Lock()

    def _get_supported_features(self):
        supported = []

        if self._is_command_supported(CONF_COMMAND_POWER_ON):
            supported.append(MediaPlayerEntityFeature.TURN_ON)
        if self._is_command_supported(
                CONF_COMMAND_POWER_ON):
            supported.append(MediaPlayerEntityFeature.TURN_OFF)
        if self._is_command_supported(
                CONF_COMMAND_VOLUME_MUTE):
            supported.append(MediaPlayerEntityFeature.VOLUME_MUTE)
        if self._is_command_supported(
                CONF_COMMAND_VOLUME_UP) and self._is_command_supported(CONF_COMMAND_VOLUME_DOWN):
            supported.append(MediaPlayerEntityFeature.VOLUME_STEP)
            supported.append(MediaPlayerEntityFeature.VOLUME_SET)
        if self._is_command_supported(CONF_COMMAND_PAUSE):
            supported.append(MediaPlayerEntityFeature.PAUSE)
        if self._is_command_supported(CONF_COMMAND_PLAY):
            supported.append(MediaPlayerEntityFeature.PLAY)
        if self._is_command_supported(CONF_COMMAND_STOP):
            supported.append(MediaPlayerEntityFeature.STOP)
        if self._is_command_supported(
                CONF_COMMAND_NEXT_TRACK):
            supported.append(MediaPlayerEntityFeature.NEXT_TRACK)
        if self._is_command_supported(
                CONF_COMMAND_PREVIOUS_TRACK):
            supported.append(MediaPlayerEntityFeature.PREVIOUS_TRACK)
        if len(self._attr_source_list) > 0:
            supported.append(MediaPlayerEntityFeature.SELECT_SOURCE)

        return functools.reduce(operator.or_, supported)

    async def async_added_to_hass(self) -> None:
        """Register callbacks."""
        await super().async_added_to_hass()
        await self._load_state_from_store()

    async def async_will_remove_from_hass(self) -> None:
        """Remove callbacks."""
        await super().async_will_remove_from_hass()

    async def _load_state_from_store(self):
        stored_data = await self._store.async_load()
        if stored_data:
            data = stored_data.get(self.entity_id)
            if data:
                self._attr_volume_level = data.get(STORAGE_CURRENT_VOLUME)
                self._attr_is_volume_muted = data.get(STORAGE_IS_MUTED)
                if not self._use_power_template:
                    self._attr_is_on = self._attr_is_volume_muted = data.get(STORAGE_IS_POWERED)
                if len(self._attr_source_list) > 0:
                    self._attr_source = data.get(STORAGE_SELECTED_SOURCE)
            _LOGGER.debug(
                "Loaded data from store for '%s': volume level: %s, volume muted: %s, is_powered: %s, source: %s",
                self.entity_id, self._attr_volume_level, self._attr_is_volume_muted, self._attr_is_on,
                self._attr_source)

    async def _update_state_in_store(self):
        _LOGGER.debug("Saving data to store for '%s'...", self.entity_id)
        stored_data = await self._store.async_load()
        if stored_data is None:
            stored_data = {}
        data = stored_data.get(self.entity_id)
        if data is None:
            data = {}
        data[STORAGE_CURRENT_VOLUME] = self._attr_volume_level
        data[STORAGE_IS_MUTED] = self._attr_is_volume_muted
        if not self._use_power_template:
            data[STORAGE_IS_POWERED] = self._attr_is_on
        if len(self._attr_source_list) > 0:
            data[STORAGE_SELECTED_SOURCE] = self._attr_source
        stored_data[self.entity_id] = data
        await self._store.async_save(stored_data)

    @property
    def state(self) -> MediaPlayerState:
        """Return the state of the device."""
        if self._attr_is_on:
            return MediaPlayerState.ON
        return MediaPlayerState.OFF

    async def async_turn_on(self) -> None:
        """Turn the device on."""
        if not self._attr_is_on:
            await self._send_key_commands([CONF_COMMAND_POWER_ON])
            if not self._use_power_template:
                self._attr_is_on = True
                await self._update_state_in_store()

    async def async_turn_off(self) -> None:
        """Turn the device off."""
        if self._attr_is_on:
            command = CONF_COMMAND_POWER_OFF if self._is_command_supported(
                CONF_COMMAND_POWER_OFF) else CONF_COMMAND_POWER_ON
            await self._send_key_commands([command])
            if not self._use_power_template:
                self._attr_is_on = False
                await self._update_state_in_store()

    async def async_volume_up(self) -> None:
        """Turn volume up for media player."""
        volume = self._attr_volume_level + self._volume_step
        await self.async_set_volume_level(min(1, volume))

    async def async_volume_down(self) -> None:
        """Turn volume down for media player."""
        volume = self._attr_volume_level - self._volume_step
        await self.async_set_volume_level(max(0, volume))

    async def async_set_volume_level(self, volume):
        """Updates the currently saved volume to the specific value"""
        requested_volume = round(volume / self._volume_step, 0) * self._volume_step
        iterations = round((requested_volume - self._attr_volume_level) / self._volume_step)
        command = CONF_COMMAND_VOLUME_UP
        if iterations < 0:
            command = CONF_COMMAND_VOLUME_DOWN
            iterations = abs(iterations)

        if iterations > 0:
            _LOGGER.debug(
                "Changing the volume on the entity %s. Current volume: %s, Requested volume: %s. Calling command '%s' %s times...",
                self.entity_id, self._attr_volume_level, requested_volume, command, iterations)
            self._attr_volume_level = requested_volume
            commands = []
            commands.extend([command] * iterations)
            await self._send_key_commands(commands)
            self.async_write_ha_state()
            await self._update_state_in_store()

    async def async_mute_volume(self, mute: bool) -> None:
        """Mute the volume."""
        if mute != self.is_volume_muted:
            await self._send_key_commands([CONF_COMMAND_VOLUME_MUTE])
            await self._update_state_in_store()

    async def async_media_play(self) -> None:
        """Send play command."""
        await self._send_key_commands([CONF_COMMAND_PLAY])

    async def async_media_pause(self) -> None:
        """Send pause command."""
        command = CONF_COMMAND_PAUSE if self._is_command_supported(CONF_COMMAND_PAUSE) else CONF_COMMAND_PLAY
        await self._send_key_commands([command])

    async def async_media_stop(self) -> None:
        """Send stop command."""
        await self._send_key_commands([CONF_COMMAND_STOP])

    async def async_media_previous_track(self) -> None:
        """Send previous track command."""
        await self._send_key_commands([CONF_COMMAND_PREVIOUS_TRACK])

    async def async_media_next_track(self) -> None:
        """Send next track command."""
        await self._send_key_commands([CONF_COMMAND_NEXT_TRACK])

    async def async_select_source(self, source):
        self._attr_source = source
        async with self._temp_lock:
            await self._change_source(source)
            await asyncio.sleep(0.5)
        self.async_write_ha_state()
        await self._update_state_in_store()

    async def _send_key_commands(
            self, commands: list[str], delay_secs: float = 0.5
    ) -> None:
        """Send a key press sequence to the remote."""
        async with self._temp_lock:
            for command_name in commands:
                await self._execute_command(command_name)
                await asyncio.sleep(delay_secs)
